..
    :copyright: Copyright (c) 2014 ftrack

.. _api_reference:

*************
API reference
*************

:mod:`bitdock`
=============

.. automodule:: bitdock

.. automodule:: bitdock.__main__


